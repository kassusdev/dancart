import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { AppRoutingModule } from "./app-routing.module";
import { CommonModule } from "@angular/common";

import { MenubarModule } from "primeng/menubar";
import { MenuItem } from "primeng/api";
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";
import { SharedModule } from "primeng/api";
import { CarouselModule } from "primeng/carousel";
import { ToolbarModule } from "primeng/toolbar";
import { SplitButtonModule } from "primeng/splitbutton";
import { FieldsetModule } from "primeng/fieldset";
import { SelectItem } from "primeng/api";
import { MessageService } from "primeng/api";
import { ToastModule } from "primeng/toast";
import { RippleModule } from "primeng/ripple";
import { PrimeNGConfig } from "primeng/api";
import { TableModule } from "primeng/table";
import { ChartModule } from "primeng/chart";
import { PanelModule } from "primeng/panel";
import { GMapModule } from "primeng/gmap";
import { DialogModule } from "primeng/dialog";
import { CheckboxModule } from "primeng/checkbox";
import { DropdownModule } from "primeng/dropdown";
import { AppToolbarComponent } from "./features/app-toolbar/app-toolbar.component";

import { CustomHtmlLabelService } from "./shared/customHtmlLabel/custom-html-label.service";
import { EnvService } from "./shared/env/env.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
  providers: [MessageService],
  styles: [
    `
      :host ::ng-deep .p-cell-editing {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    `,
  ],
})
export class AppComponent {
  title = "dancart";
  constructor(
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    private env: EnvService,
    private customHtmlLabelService: CustomHtmlLabelService
  ) {}

  ngOnInit() {
    this.primengConfig.ripple = true;
  }
}
