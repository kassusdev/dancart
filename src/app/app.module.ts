import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { HttpClientXsrfModule } from "@angular/common/http";
import {
  RequestCache,
  RequestCacheWithMap,
} from "../app/features/services/request-cache.service";
import { HttpErrorHandler } from "../app/features/services/http-error-handler.service";
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MessagesService } from "../app/features/services/message.service";

import { MenubarModule } from "primeng/menubar";
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";
import { SharedModule } from "primeng/api";
import { CarouselModule } from "primeng/carousel";
import { ToolbarModule } from "primeng/toolbar";
import { SplitButtonModule } from "primeng/splitbutton";
import { FieldsetModule } from "primeng/fieldset";
import { MessageService } from "primeng/api";
import { ToastModule } from "primeng/toast";
import { RippleModule } from "primeng/ripple";
import { TableModule } from "primeng/table";
import { ChartModule } from "primeng/chart";
import { PanelModule } from "primeng/panel";
import { GMapModule } from "primeng/gmap";
import { DialogModule } from "primeng/dialog";
import { CheckboxModule } from "primeng/checkbox";
import { DropdownModule } from "primeng/dropdown";

import { AppComponent } from "./app.component";
import { HomeComponent } from "./features/home/home.component";
import { PageNotFoundComponent } from "../app/features/page-not-found/page-not-found.component";
import { AdminComponent } from "./features/admin/admin.component";
import { MapComponent } from "./features/map/map.component";
import { FirstPageComponent } from "./features/first-page/first-page.component";
import { ReportComponent } from "./features/report/report.component";
import { AppToolbarComponent } from "./features/app-toolbar/app-toolbar.component";
import { ContactComponent } from "./features/contact/contact.component";

import { environment } from "../environments/environment";
//import { ServiceWorkerModule } from '@angular/service-worker';
import { EnvServiceProvider } from "../app/shared/env/env.service.provider";
import { AuthService } from "../app/features/services/auth/auth.service";
import { CustomHtmlLabelService } from "../app/shared/customHtmlLabel/custom-html-label.service";
import { httpInterceptorProviders } from "./features/services/http-interceptors";

@NgModule({
  declarations: [
    AppToolbarComponent,
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    AdminComponent,
    MapComponent,
    ReportComponent,
    FirstPageComponent,
    ContactComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MenubarModule,
    InputTextModule,
    ButtonModule,
    SharedModule,
    CarouselModule,
    ToolbarModule,
    SplitButtonModule,
    FieldsetModule,
    ToastModule,
    RippleModule,
    FormsModule,
    TableModule,
    ChartModule,
    PanelModule,
    GMapModule,
    DialogModule,
    CommonModule,
    ReactiveFormsModule,
    // ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    HttpClientXsrfModule.withOptions({
      cookieName: "My-Xsrf-Cookie",
      headerName: "My-Xsrf-Header",
    }),
    DropdownModule,
  ],
  providers: [
    AuthService,
    HttpErrorHandler,
    MessageService,
    MessagesService,
    CustomHtmlLabelService,
    { provide: RequestCache, useClass: RequestCacheWithMap },
    httpInterceptorProviders,
    EnvServiceProvider,
  ],

  bootstrap: [AppComponent],
})
export class AppModule {}
