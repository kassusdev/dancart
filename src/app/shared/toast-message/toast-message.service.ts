import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class ToastMessageService {
  constructor() {}

  ToastMessage = {
    contacts_success: {
      key: "Contacts",
      severity: "success",
      summary: "Contacter",
      detail: "Le message a bien été envoyé à l'administrteur",
      sticky: true,
    },
    contacts_failure: {
      key: "Contacts",
      severity: "error",
      summary: "Contacter",
      detail:
        "Un problème est survenu lors de l'envoie du message à l'administrteur",
      sticky: true,
    },
    users_success: {
      key: "Utilisateurs",
      severity: "success",
      summary: "Administrer",
      detail: "La liste de tous les utilisateurs a bien été récupéré",
      sticky: true,
    },
    users_failure: {
      key: "Utilisateurs",
      severity: "error",
      summary: "Administrer",
      detail:
        "Un problème est survenu lors de la récupération de la liste des utilisateurs",
      sticky: true,
    },
    users_add_success: {
      key: "Utilisateurs",
      severity: "success",
      summary: "Administrer",
      detail:
        "Une nouvelle ligne vierge a bien été ajoutée au tableau des utilisateurs",
      sticky: true,
    },
    users_add_failure: {
      key: "Utilisateurs",
      severity: "error",
      summary: "Administrer",
      detail: "Un problème est survenu lors de l'ajout du nouvel utilisateur",
      sticky: true,
    },
    users_del_success: {
      key: "Utilisateurs",
      severity: "success",
      summary: "Administrer",
      detail: "L'utilisateur a bien été supprimé",
      sticky: true,
    },
    users_del_failure: {
      key: "Utilisateurs",
      severity: "error",
      summary: "Administrer",
      detail: "Un problème est survenu lors de la suppression de l'utilisateur",
      sticky: true,
    },
    users_put_success: {
      key: "Utilisateurs",
      severity: "success",
      summary: "Administrer",
      detail: "L'utilisateur a bien été modifié",
      sticky: true,
    },
    users_put_failure: {
      key: "Utilisateurs",
      severity: "error",
      summary: "Administrer",
      detail:
        "Un problème est survenu lors de la modification de l'utilisateur",
      sticky: true,
    },
    reports_success: {
      key: "Rapports",
      severity: "success",
      summary: "Reporting",
      detail: "La liste des rapports a bien été récupéré",
      sticky: true,
    },
    reports_failure: {
      key: "Rapports",
      severity: "error",
      summary: "Reporting",
      detail:
        "Un problème est survenu lors de lla récupération de la liste des rapports",
      sticky: true,
    },
    maps_success: {
      key: "Carte",
      severity: "success",
      summary: "Liste des Markers",
      detail: "La liste de tous les markers a bien été récupéré",
      sticky: true,
    },
    maps_failure: {
      key: "Carte",
      severity: "error",
      summary: "Liste des cartes",
      detail:
        "Un problème est survenu lors de la récupération de la liste des markers",
      sticky: true,
    },
    maps_add_success: {
      key: "Carte",
      severity: "success",
      summary: "Ajout d'un marker",
      detail: "Le nouveau marker a bien été ajoutée dans la base de données",
      sticky: true,
    },
    maps_add_failure: {
      key: "Carte",
      severity: "error",
      summary: "Ajout d'un marker",
      detail: "Un problème est survenu lors de l'ajout du nouveau marker",
      sticky: true,
    },
    maps_del_success: {
      key: "Carte",
      severity: "success",
      summary: "Suppression  marker",
      detail: "Le marker a bien été supprimé",
      sticky: true,
    },
    maps_del_failure: {
      key: "Carte",
      severity: "error",
      summary: "Suppression",
      detail:
        "Un problème est survenu lors de la suppression du nouveau marker",
      sticky: true,
    },
    maps_put_success: {
      key: "Carte",
      severity: "success",
      summary: "Modifier un marker",
      detail: "Le marker  a bien été modifié",
      sticky: true,
    },
    maps_put_failure: {
      key: "Carte",
      severity: "error",
      summary: "Modifier un marker",
      detail: "Un problème est survenu lors de la modification du marker",
      sticky: true,
    },
  };
}
