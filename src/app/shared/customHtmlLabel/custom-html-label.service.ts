import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class CustomHtmlLabelService {
  constructor() {}

  public admin = "Administer";
  public off = "Déconnexion";
  public search = "Rechercher";
  public report = "Reporting";
  public map = "Carte";
  public back = "Retour";
  public login = "Connexion";
  public contact = "Contact";
}
