import { TestBed } from '@angular/core/testing';

import { CustomHtmlLabelService } from './custom-html-label.service';

describe('CustomHtmlLabelService', () => {
  let service: CustomHtmlLabelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomHtmlLabelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
