import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './features/home/home.component';
import { AdminComponent } from '../app/features/admin/admin.component';
import { MapComponent } from '../app/features/map/map.component';
import { ReportComponent } from '../app/features/report/report.component';
import { FirstPageComponent } from '../app/features/first-page/first-page.component';
import { ContactComponent } from '../app/features/contact/contact.component';
import { PageNotFoundComponent } from '../app/features/page-not-found/page-not-found.component';
import { AppToolbarComponent } from '../app/features/app-toolbar/app-toolbar.component';

/**
 * define all component routes
 */

const routes: Routes = [
  { path: '', component: FirstPageComponent },
  { path: 'home', component: HomeComponent },
  { path: 'toolbar', component: AppToolbarComponent },
  { path: 'first', component: FirstPageComponent },
  { path: 'map', component: MapComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'report', component: ReportComponent },
  { path: '', redirectTo: '/first', pathMatch: 'full' }, // redirect to `FirstPageComponent`
  // otherwise redirect to PageNotFoundComponent
  { path: 'page', component: PageNotFoundComponent }, // Wildcard route for a 404 page
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true, enableTracing: true }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }