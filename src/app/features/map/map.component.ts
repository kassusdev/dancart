import { Component, Injectable, OnInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MenubarModule } from "primeng/menubar";
import { MenuItem } from "primeng/api";
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";
import { SharedModule } from "primeng/api";
import { PrimeNGConfig } from "primeng/api";
import { ToolbarModule } from "primeng/toolbar";
import { CustomHtmlLabelService } from "../../shared/customHtmlLabel/custom-html-label.service";
import { GMapModule } from "primeng/gmap";
import { MessageService } from "primeng/api";
import { DialogModule } from "primeng/dialog";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";

import { Maps } from "../models/maps";

import { ToastMessageService } from "../../shared/toast-message/toast-message.service";
import { MapsService } from "../services/maps/maps.service";

declare var google: any;

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css", "../../shared/global.css"],
  providers: [CustomHtmlLabelService],
  styles: [
    `
      .p-col-2 {
        display: flex;
        align-self: center;
      }
    `,
  ],
})
export class MapComponent implements OnInit {
  public options: any;

  public google: any;

  public overlays: any[];

  public dialogVisible: boolean;

  public markerTitle: string;
  public newMarker: Maps;
  public marker: Maps;

  public selectedPosition: any;

  public infoWindow: any;

  public draggable: boolean;

  // define mapss array and standlone map object

  public maps: Maps[];

  public map: Maps;

  // Define for checking the success or failure response of the restfull api

  public isSuccess = "";

  // Define for checking if the use want to edit the marker form

  public iseditable = "";

  // define userForm variables

  public mapsForm: FormGroup;
  public mapsEditForm: FormGroup;
  public teachername: FormControl;
  public teacherimg: FormControl;
  public roomlng: FormControl;
  public roomlat: FormControl;
  public markertitle: FormControl;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private toast: ToastMessageService,
    public customHtmlLabelService: CustomHtmlLabelService,
    private mapsService: MapsService,
    private primengConfig: PrimeNGConfig
  ) {}

  ngOnInit(): void {
    this.options = {
      center: { lat: 36.890257, lng: 30.707417 },
      zoom: 12,
    };

    // Init fake data for testing
    this.maps = [
      {
        id: 1,
        teachername: "kass",
        teacherimg: "test image",
        roomlng: 2.4206,
        roomlat: 48.8229,
        markertitle: "test marker title",
        draggable: true,
      },
      {
        id: 2,
        teachername: "kass2",
        teacherimg: "test image",
        roomlng: 2.4205,
        roomlat: 48.8227,
        markertitle: "test marker title",
        draggable: true,
      },
      {
        id: 3,
        teachername: "kass3",
        teacherimg: "test image",
        roomlng: 2.4203,
        roomlat: 48.8225,
        markertitle: "test marker title",
        draggable: true,
      },
    ];

    // get the all existing maps

    //this.getAllMaps();

    /**
     * This is for initializing the mapsform
     */

    this.mapsForm = new FormGroup({
      teachername: new FormControl(""),
      teacherimg: new FormControl(""),
      roomlng: new FormControl(""),
      roomlat: new FormControl(""),
      markertitle: new FormControl(""),
      draggable: new FormControl(""),
    });

    /**
     * This is for initializing the mapsform
     */

    this.mapsEditForm = new FormGroup({
      teachername: new FormControl(""),
      teacherimg: new FormControl(""),
      roomlng: new FormControl(""),
      roomlat: new FormControl(""),
      markertitle: new FormControl(""),
      draggable: new FormControl(""),
    });

    this.initOverlays();

    this.infoWindow = new google.maps.InfoWindow();
  }

  handleMapClick(event) {
    this.dialogVisible = true;
    this.selectedPosition = event.latLng;
    this.roomlat = this.selectedPosition.lat();
    this.roomlng = this.selectedPosition.lng();
  }

  handleOverlayClick(event) {
    let isMarker = event.overlay.getTitle != undefined;

    if (isMarker) {
      let title = event.overlay.getTitle();
      this.infoWindow.setContent("" + title + "");
      this.infoWindow.open(event.map, event.overlay);
      event.map.setCenter(event.overlay.getPosition());

      this.messageService.add({
        severity: "info",
        summary: "le marker est sélectionné",
        detail: title,
      });
    } else {
      this.messageService.add({
        severity: "info",
        summary: "Problème de sélection du marker",
        detail: "",
      });
    }
  }

  /**
   * This is use for adding a new marker
   * @param newMarker
   *
   */

  onSubmit(newMarker: Maps) {
    this.overlays.push(
      new google.maps.Marker({
        position: {
          lat: this.roomlat,
          lng: this.roomlng,
        },
        title: this.markerTitle,
        draggable: this.draggable,
      })
    );
    // call the restfull api for adding a new maps (marker)
    this.mapsService.addMap(newMarker).subscribe(
      (map) => {
        this.isSuccess = "true";
        // push the retriving  maps into maps array
        this.maps.push(map);
        // display toast message
        this.showAddToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showAddToast(this.isSuccess);
      }
    );

    this.markerTitle = "";
    this.dialogVisible = false;
  }

  handleDragEnd(event) {
    this.messageService.add({
      severity: "info",
      summary: "Marker Dragged",
      detail: event.overlay.getTitle(),
    });
  }

  initOverlays() {
    if (!this.overlays || !this.overlays.length) {
      for (let i = 0; i < this.maps.length; i++) {
        const map = this.maps[i];
        this.overlays = [
          new google.maps.Marker({
            position: { lat: map.roomlat, lng: map.roomlng },
            title: map.markertitle,
          }),

          //TODO: The real init geo coords will be define there
          new google.maps.Polygon({
            paths: [
              { lat: 48.8229, lng: 2.4306 },
              { lat: 47.8229, lng: 2.606 },
              { lat: 46.8229, lng: 2.4406 },
              { lat: 45.8229, lng: 2.4406 },
            ],
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: "#1976D2",
            fillOpacity: 0.35,
          }),

          //TODO: The real init google map circle will be define there
          new google.maps.Circle({
            center: { lat: 48.8229, lng: 2.4306 },
            fillColor: "#1976D2",
            fillOpacity: 0.35,
            strokeWeight: 1,
            radius: 1500,
          }),

          //TODO: The real init google map polyline will be define there
          new google.maps.Polyline({
            path: [
              { lat: 48.8229, lng: 2.4309 },
              { lat: 48.8329, lng: 2.431 },
            ],
            geodesic: true,
            strokeColor: "#FF0000",
            strokeOpacity: 0.5,
            strokeWeight: 2,
          }),
        ];
      }
    }
  }
  /**
   *
   * @param map
   * This use for zooming in
   */

  zoomIn(map) {
    map.setZoom(map.getZoom() + 1);
  }

  /**
   *
   * @param map
   * This use for zooming out
   */
  zoomOut(map) {
    map.setZoom(map.getZoom() - 1);
  }

  /**
   *
   *This use for deleteing all disply marker on the map
   */

  clear() {
    this.overlays = [];
  }

  /**
   * This is use for getting the all  existing maps
   */

  getAllMaps(): void {
    // call the restfull api for getting all existing maps
    this.mapsService.getAllMaps().subscribe(
      (maps) => {
        this.isSuccess = "true";
        // push the retriving  maps into maps array
        this.maps = maps;
        // display toast message
        this.showToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showToast(this.isSuccess);
      }
    );
  }

  /**
   * This is use for toggling the edit form
   */

  toggleEditMode(): void {
    this.iseditable = "true";
  }

  /**
   * Method for editing the maps array
   * This is use to update the maps array
   * @param map
   */

  editMap(map: Maps): void {
    // call the restfull api for updating a selected map
    this.mapsService.updateMap(map).subscribe(
      (map) => {
        this.isSuccess = "true";
        // replace the user in the users list with update from server
        const ix = map ? this.maps.findIndex((h) => h.id === map.id) : -1;
        if (ix > -1) {
          this.maps[ix] = map;
        }
        // display toast message
        this.showEditToast(this.isSuccess);
        // disable editMode
        this.iseditable = "true";
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showEditToast(this.isSuccess);
        // disable editMode
        this.iseditable = "true";
      }
    );
  }

  /**
   * Method for deleting a selected map
   * This is use for deleting the selected marker
   * @param map
   */
  deleteMap(map: Maps): void {
    // call the restfull api for deleting  a selected user
    this.mapsService.deleteMap(map).subscribe(
      (seletedMarker) => {
        this.isSuccess = "true";
        // delete value from models
        const idxToUpdate = this.maps.findIndex((value) => value.id === map.id);
        this.maps.splice(idxToUpdate, 1);
        // display toast message
        this.showDelToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showDelToast(this.isSuccess);
      }
    );
  }

  /**
   * this is use for displaying  toast message
   */
  showToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_failure);
    }
  }

  /**
   * this is use for displaying  toast message
   */
  showAddToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_add_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_add_failure);
    }
  }

  /**
   * this is use for displaying  toast message
   */
  showEditToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_put_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_put_failure);
    }
  }

  /**
   * this is use for displaying  toast message
   */
  showDelToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_del_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.maps_del_failure);
    }
  }
}
