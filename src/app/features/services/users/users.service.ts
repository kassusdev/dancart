import { ApplicationRef, Injectable } from "@angular/core";
//import { SwUpdate } from '@angular/service-worker';
import { concat, interval } from "rxjs";
import { first } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { HttpClient, HttpParams } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { HttpErrorHandler, HandleError } from "../http-error-handler.service";
import { environment } from "../../../../environments/environment";

import { Users } from "../../models/users";
import { EnvService } from "src/app/shared/env/env.service";

/**
 * This is use for http request
 */
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "my-auth-token",
  }),
};

@Injectable({
  providedIn: "root",
})
export class UsersService {
  private handleError: HandleError;
  public usersURL: string;

  constructor(
    appRef: ApplicationRef,
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandler,
    private env: EnvService
  ) {
    // Allow the app to stabilize first, before starting polling for updates with `interval()`.
    const appIsStable$ = appRef.isStable.pipe(
      first((isStable) => isStable === true)
    );
    const everySixHours$ = interval(6 * 60 * 60 * 1000);
    const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);

    //everySixHoursOnceAppIsStable$.subscribe(() => updates.checkForUpdate());

    this.usersURL = this.env.apiBaseUrl;
    this.handleError = httpErrorHandler.createHandleError("UsersService");
  }

  /**
   *
   * GET: get all users from  the database
   */
  getAllUsers(): Observable<Users[]> {
    //TODO: add the optionnel variable in  the get url
    const contactsURL = `${this.usersURL}`;
    return this.http
      .get<Users[]>(this.usersURL, httpOptions)
      .pipe(catchError(this.handleError("getAllUsers", [])));
  }

  /**
   *
   * GET: get all teachers from  the database
   */
  getAllTeachers(): Observable<Users[]> {
    //TODO: add the optionnel variable in  the get url
    const usersURL = `${this.usersURL}`;
    return this.http
      .get<Users[]>(this.usersURL, httpOptions)
      .pipe(catchError(this.handleError("getAllTeachers", [])));
  }

  /**
   *
   * @param user
   *  POST: add a new user to the database
   * */

  addUsers(user: Users): Observable<Users> {
    //TODO: add the optionnel variable in  the post url
    const usersURL = `${this.usersURL}`;
    return this.http
      .post<Users>(this.usersURL, user, httpOptions)
      .pipe(catchError(this.handleError("addUser", user)));
  }

  /**
   *
   * @param id
   * DELETE: delete the user from the server
   */
  deleteUser(user: Users): Observable<{}> {
    //TODO: add the optionnel variable in  the delete url
    const usersURL = `${this.usersURL}/${user.id}`;
    return this.http
      .delete(usersURL, httpOptions)
      .pipe(catchError(this.handleError("deleteUser", user)));
  }

  /**
   *
   * @param user
   * PUT: update the user on the server. Returns the updated user upon success.
   */
  updateUser(user: Users): Observable<Users> {
    //TODO: add the optionnel variable in  the put url
    httpOptions.headers = httpOptions.headers.set(
      "Authorization",
      "my-new-auth-token"
    );

    return this.http
      .put<Users>(this.usersURL, user, httpOptions)
      .pipe(catchError(this.handleError("updateUser", user)));
  }
}
