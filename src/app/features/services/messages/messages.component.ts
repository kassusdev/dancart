import { Component } from '@angular/core';
import { MessagesService } from '../message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html'
})
export class MessagesComponent {
  constructor(public messagesService: MessagesService) { }
}
