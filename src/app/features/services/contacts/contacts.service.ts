
import { ApplicationRef, Injectable } from '@angular/core';
//import { SwUpdate } from '@angular/service-worker';
import { concat, interval } from 'rxjs';
import { first } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpErrorHandler, HandleError } from '../http-error-handler.service';
import { environment } from '../../../../environments/environment';

import { Contacts } from "../../models/contacts";
import { EnvService } from 'src/app/shared/env/env.service';


/**
 * This is use for http request 
 */
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};


@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  private handleError: HandleError;
  public contactsURL: string;


  constructor(appRef: ApplicationRef, private http: HttpClient, private httpErrorHandler: HttpErrorHandler, private env: EnvService) {
    // Allow the app to stabilize first, before starting polling for updates with `interval()`.
    const appIsStable$ = appRef.isStable.pipe(first(isStable => isStable === true));
    const everySixHours$ = interval(6 * 60 * 60 * 1000);
    const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);

    //everySixHoursOnceAppIsStable$.subscribe(() => updates.checkForUpdate());

    this.contactsURL = this.env.apiBaseUrl;
    this.handleError = httpErrorHandler.createHandleError('ContactsService');

  }



  /**
   * 
   * @param contacts 
   * GET: get all contacts from  the database
   */
  getAllContacts(): Observable<Contacts[]> {
    //TODO: add the optionnel variable in  the post url
    const contactsURL = `${this.contactsURL}`;
    return this.http.get<Contacts[]>(this.contactsURL, httpOptions)
      .pipe(
        catchError(this.handleError('getAllContacts', []))
      );
  }

  /**
   * 
   * @param contact 
   *  POST: add a new hero to the database 
   * */

  addContact(contact: Contacts): Observable<Contacts> {

    //TODO: add the optionnel variable in  the post url
    const contactsURL = `${this.contactsURL}`;
    return this.http.post<Contacts>(this.contactsURL, contact, httpOptions)
      .pipe(
        catchError(this.handleError('addContact', contact))
      );
  }
}
