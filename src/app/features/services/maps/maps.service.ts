import { ApplicationRef, Injectable } from "@angular/core";
//import { SwUpdate } from '@angular/service-worker';
import { concat, interval } from "rxjs";
import { first } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { HttpClient, HttpParams } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { HttpErrorHandler, HandleError } from "../http-error-handler.service";
import { environment } from "../../../../environments/environment";

import { Maps } from "../../models/maps";
import { EnvService } from "src/app/shared/env/env.service";

/**
 * This is use for http request
 */
const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json",
    Authorization: "my-auth-token",
  }),
};
@Injectable({
  providedIn: "root",
})
export class MapsService {
  private handleError: HandleError;
  public mapsURL: string;

  constructor(
    appRef: ApplicationRef,
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandler,
    private env: EnvService
  ) {
    // Allow the app to stabilize first, before starting polling for updates with `interval()`.
    const appIsStable$ = appRef.isStable.pipe(
      first((isStable) => isStable === true)
    );
    const everySixHours$ = interval(6 * 60 * 60 * 1000);
    const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);

    //everySixHoursOnceAppIsStable$.subscribe(() => updates.checkForUpdate());

    this.mapsURL = this.env.apiBaseUrl;
    this.handleError = httpErrorHandler.createHandleError("MapsService");
  }

  /**
   *
   * GET: get all maps from  the database
   */
  getAllMaps(): Observable<Maps[]> {
    //TODO: add the optionnel variable in  the get url
    const mapsURL = `${this.mapsURL}`;
    return this.http
      .get<Maps[]>(this.mapsURL, httpOptions)
      .pipe(catchError(this.handleError("getAllMaps", [])));
  }

  /**
   *
   * @param map
   *  POST: add a new user to the database
   * */

  addMap(map: Maps): Observable<Maps> {
    //TODO: add the optionnel variable in  the post url
    const mapsURL = `${this.mapsURL}`;
    return this.http
      .post<Maps>(this.mapsURL, map, httpOptions)
      .pipe(catchError(this.handleError("addMap", map)));
  }

  /**
   *
   * @param id
   * DELETE: delete the map from the server
   */
  deleteMap(map: Maps): Observable<{}> {
    //TODO: add the optionnel variable in  the delete url
    const mapsURL = `${this.mapsURL}/${map.id}`;
    return this.http
      .delete(mapsURL, httpOptions)
      .pipe(catchError(this.handleError("deleteMap", map)));
  }

  /**
   *
   * @param map
   * PUT: update the map on the server. Returns the updated map upon success.
   */
  updateMap(map: Maps): Observable<Maps> {
    //TODO: add the optionnel variable in  the put url
    httpOptions.headers = httpOptions.headers.set(
      "Authorization",
      "my-new-auth-token"
    );

    return this.http
      .put<Maps>(this.mapsURL, map, httpOptions)
      .pipe(catchError(this.handleError("updateMap", map)));
  }
}
