
import { ApplicationRef, Injectable } from "@angular/core";
//import { SwUpdate } from '@angular/service-worker';
import { concat, interval } from "rxjs";
import { first } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from "rxjs/operators";
import { HttpClient, HttpParams } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { HttpErrorHandler, HandleError } from "../http-error-handler.service";
import { environment } from "../../../../environments/environment";

import { Users } from "../../models/users";
import { Reports } from "../../models/reports";

import { UsersService } from "../../services/users/users.service";

//TODO: import the cards service

import { EnvService } from "src/app/shared/env/env.service";

/**
 * This is use for http request 
 */
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};


@Injectable({
  providedIn: "root",
})
export class ReportsService {
  public reportsURL: string;
  private handleError: HandleError;

  constructor(
    appRef: ApplicationRef,
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandler,
    private env: EnvService,
    private usersService: UsersService
  ) {
    // Allow the app to stabilize first, before starting polling for updates with `interval()`.
    const appIsStable$ = appRef.isStable.pipe(
      first((isStable) => isStable === true)
    );
    const everySixHours$ = interval(6 * 60 * 60 * 1000);
    const everySixHoursOnceAppIsStable$ = concat(appIsStable$, everySixHours$);

    //everySixHoursOnceAppIsStable$.subscribe(() => updates.checkForUpdate());

    this.reportsURL = this.env.apiBaseUrl;
    this.handleError = httpErrorHandler.createHandleError("ReportsService");
  }

  /**
   *
   * @param reports
   * GET: get all reports from  the database
   */
  getAllReports(): Observable<Reports[]> {
    //TODO: add the optionnel variable in  the get url
    const reportsURL = `${this.reportsURL}`;
    return this.http
      .get<Reports[]>(this.reportsURL, httpOptions)
      .pipe(catchError(this.handleError("getAllReports", [])));
  }
}
