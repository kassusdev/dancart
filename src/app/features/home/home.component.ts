import { Component, OnInit } from '@angular/core';

import { MenubarModule } from 'primeng/menubar';
import { MenuItem } from 'primeng/api';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SharedModule } from 'primeng/api';
import { CustomHtmlLabelService } from '../../shared/customHtmlLabel/custom-html-label.service';

import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../../shared/global.css'],
  providers: [CustomHtmlLabelService],
})
export class HomeComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, public customHtmlLabelService: CustomHtmlLabelService) { }

  ngOnInit() {

  }

  /**
   * Login if the user click on the button
   * submiting the login request
   */
  handleClick() { }
}
