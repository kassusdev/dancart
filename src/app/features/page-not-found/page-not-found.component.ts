import { Component, OnInit } from '@angular/core';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { SharedModule } from 'primeng/api';
import { CustomHtmlLabelService } from '../../shared/customHtmlLabel/custom-html-label.service';

import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css', '../../shared/global.css'],
})
export class PageNotFoundComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, public customHtmlLabelService: CustomHtmlLabelService) { }

  ngOnInit(): void {

  }

  /**
   * Reset login status
   *  Call  back method redirection to home page
   * */
  back() {
    //Get return url from route parameters or default to '/login'
    this.router.navigate(['/first']);
    // this.toastr.success("La déconnexion a bien été effectuée ", "Home !");
  }
}
