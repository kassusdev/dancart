import { Users } from "../models/users";

export interface UserDatas {
    id: number,
    teacher: Users,
    firstname: string,
    lastname: string,
    userfile: string
}
