export interface Maps {
  id: number;
  teachername: string;
  teacherimg: string;
  roomlng: number;
  roomlat: number;
  markertitle: string;
  draggable: Boolean;
}
