export interface Cards {
    id: number,
    cardtype: string,
    nbcard: number,
    nbcardcount: number
}
