import { Cards } from "../models/cards";

export interface Users {
  id: number;
  firstname: string;
  lastname: string;
  username: string;
  userCard: Cards[];
  userCurrentCard_id: number;
  email: string;
  password: string;
  nbcourses: number;
  isteacher: boolean;
}
