import { Users } from "../models/users";
import { Cards } from "../models/cards";

export interface Reports {
  id: number;
  nbcards: Cards[];
  nbusers: Users[];
  nbteachers: Users[];
  nbcourses: [];
}
