import { Component, OnInit } from '@angular/core';
import { CarouselModule } from 'primeng/carousel';
import { CustomHtmlLabelService } from '../../shared/customHtmlLabel/custom-html-label.service';

import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.css', '../../shared/global.css'],
  providers: [CustomHtmlLabelService],
})
export class FirstPageComponent implements OnInit {
  public responsiveOptions: CarouselModule[];
  public images = [{}];

  constructor(private route: ActivatedRoute, private router: Router, public customHtmlLabelService: CustomHtmlLabelService) {
    this.responsiveOptions = [
      {
        breakpoint: '1024px',
        numVisible: 3,
        numScroll: 3,
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2,
      },
      {
        breakpoint: '560px',
        numVisible: 1,
        numScroll: 1,
      },
    ];
    this.images = [
      {
        id: 1,
        url:
          '/home/lenny/Documents/Project/dancart/src/assets/images/index.jpeg',
      },
      {
        id: 2,
        url:
          '/home/lenny/Documents/Project/dancart/src/assets/images/index2.jpeg',
      },
      {
        id: 3,
        url:
          '/home/lenny/Documents/Project/dancart/src/assets/images/index3.jpeg',
      },
      {
        id: 4,
        url:
          '/home/lenny/Documents/Project/dancart/src/assets/images/index4.jpeg',
      },
      {
        id: 5,
        url:
          '/home/lenny/Documents/Project/dancart/src/assets/images/index5.jpeg',
      },
      {
        id: 6,
        url:
          '/home/lenny/Documents/Project/dancart/src/assets/images/images6.jpeg',
      },
    ];
  }

  ngOnInit() {

  }
}
