import { Component, Injectable, OnInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputTextModule } from "primeng/inputtext";
import { MenuItem } from "primeng/api";
import { ButtonModule } from "primeng/button";
import { SharedModule } from "primeng/api";
import { ToolbarModule } from "primeng/toolbar";
import { SplitButtonModule } from "primeng/splitbutton";
import { FieldsetModule } from "primeng/fieldset";
import { SelectItem } from "primeng/api";
import { MessageService } from "primeng/api";
import { RippleModule } from "primeng/ripple";
import { PrimeNGConfig } from "primeng/api";
import { TableModule } from "primeng/table";
import { DropdownModule } from "primeng/dropdown";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";

import { CustomHtmlLabelService } from "../../shared/customHtmlLabel/custom-html-label.service";
import { Users } from "../models/users";
import { Cards } from "../models/cards";

import { ToastMessageService } from "../../shared/toast-message/toast-message.service";
import { UsersService } from "../services/users/users.service";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css", "../../shared/global.css"],
  providers: [UsersService, CustomHtmlLabelService],
  styles: [
    `
      :host ::ng-deep .p-cell-editing {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    `,
  ],
})
export class AdminComponent implements OnInit {
  public isTeacher: SelectItem[];
  public selectedUser: SelectItem;

  public items: MenuItem[];

  public user: Users;

  // define users array

  public users: Users[];

  public statuses: SelectItem[];

  public userCards: Cards[];

  // Define for checking the success or failure response of the restfull api

  public isSuccess = "";

  // define userForm variables

  public usersForm: FormGroup;
  public firstname: FormControl;
  public lastname: FormControl;
  public username: FormControl;
  public userCurrentCard_id: FormControl;
  public userCard: FormControl;
  public email: FormControl;
  public nbcourses: FormControl;
  public isteacher: FormControl;
  public password: FormControl;

  public clonedUsers: { [s: string]: Users } = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private toast: ToastMessageService,
    public customHtmlLabelService: CustomHtmlLabelService,
    private usersService: UsersService,
    private primengConfig: PrimeNGConfig
  ) {}

  ngOnInit(): void {
    this.primengConfig.ripple = true;

    // init isteacher select box

    this.isTeacher = [
      { label: "Oui", value: true },
      { label: "Non", value: false },
    ];

    /**
     * This is for initializing the contactform
     */

    this.usersForm = new FormGroup({
      firstname: new FormControl(""),
      lastname: new FormControl(""),
      username: new FormControl(""),
      userCurrentCard_id: new FormControl(""),
      userCard: new FormControl(""),
      email: new FormControl(""),
      nbcourses: new FormControl(""),
      isteacher: new FormControl(""),
      password: new FormControl(""),
    });

    // get the all existing contacts

    //this.getAllUsers();
    this.users = [
      {
        id: 1,
        firstname: "string",
        lastname: "string",
        username: "string",
        userCurrentCard_id: 1,
        email: "test@gmail.com",
        nbcourses: 6,
        userCard: this.userCards,
        isteacher: true,
        password: "****"
      },
      {
        id: 2,
        firstname: "string",
        lastname: "string",
        username: "string",
        userCurrentCard_id: 1,
        email: "test@gmail.com",
        userCard: this.userCards,
        nbcourses: 6,
        isteacher: true,
        password: "****"
      },
      {
        id: 3,
        firstname: "string",
        lastname: "string",
        username: "string",
        userCurrentCard_id: 1,
        email: "test@gmail.com",
        userCard: this.userCards,
        nbcourses: 6,
        isteacher: true,
        password: "****"
      },
    ];
  }

  /**
   * This for init editing row
   */

  onRowEditInit(user: Users) {
    this.clonedUsers[user.id] = { ...user };
  }

  /**
   * This saving the edit row in the database array
   */

  onRowEditSave(user: Users, index: number) {
    // delete the cloned user
    delete this.clonedUsers[user.id];
    // upadte the selected user
    this.users[index] = user;
    this.users[index].isteacher = this.selectedUser.value;

    // update the selected user into the database
    this.editUser(user);
  }

  /**
   * This is use for cancelling the all action before
   * The clic reinit the table row
   */

  onRowEditCancel(user: Users, index: number) {
    this.users[index] = this.clonedUsers[user.id];
    // delete the cloned user
    delete this.clonedUsers[user.id];
  }

  /**
   * This is use for getting the all  existing users
   */

  getAllUsers(): void {
    // call the restfull api for getting all existing users
    this.usersService.getAllUsers().subscribe(
      (users) => {
        this.isSuccess = "true";
        // push the retriving users into users array
        this.users = users;
        // display toast message
        this.showToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showToast(this.isSuccess);
      }
    );
  }

  /**
   * Method for submitting the user form
   * This add a new user into the users array
   * * @param user
   */

  addUser(user: Users): void {
    // get the userForm value
    const emptyUser = {
      id: 0,
      firstname: "",
      lastname: "",
      username: "",
      userCard: this.userCards,
      userCurrentCard_id: 0,
      email: "",
      password: "",
      nbcourses: 0,
      isteacher: false,
      nbteachers: 0,
    };

    user = emptyUser;
    // call the restfull api for adding a new user
    this.usersService.addUsers(user).subscribe(
      (newUser) => {
        this.isSuccess = "true";

        // push the new user into users array
        this.users.push(newUser);
        //display toast message
        this.showAddToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showAddToast(this.isSuccess);
      }
    );
  }

  /**
   * Method for editing the users array
   * This is use to update the users array
   * @param user
   */

  editUser(user: Users): void {
    // call the restfull api for updating a selected user
    this.usersService.updateUser(user).subscribe(
      (user) => {
        this.isSuccess = "true";
        // replace the user in the users list with update from server
        const ix = user ? this.users.findIndex((h) => h.id === user.id) : -1;
        if (ix > -1) {
          this.users[ix] = user;
        }
        // display toast message
        this.showEditToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showEditToast(this.isSuccess);
      }
    );
  }

  /**
   * Method for deleting a selected user
   * This is use for deleting the selected user
   * @param user
   */
  deleteUser(user: Users): void {
    // call the restfull api for deleting  a selected user
    this.usersService.deleteUser(user).subscribe(
      (seletedUser) => {
        this.isSuccess = "true";
        // delete value from models
        const idxToUpdate = this.users.findIndex(
          (value) => value.id === user.id
        );
        this.users.splice(idxToUpdate, 1);
        // display toast message
        this.showDelToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showDelToast(this.isSuccess);
      }
    );
  }
  /**
   * this is use for displaying  toast message
   */
  showToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_failure);
    }
  }

  /**
   * this is use for displaying  toast message
   */
  showAddToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_add_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_add_failure);
    }
  }

  /**
   * this is use for displaying  toast message
   */
  showEditToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_put_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_put_failure);
    }
  }

  /**
   * this is use for displaying  toast message
   */
  showDelToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_del_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.users_del_failure);
    }
  }
}
