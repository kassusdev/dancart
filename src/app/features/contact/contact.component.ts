import { Component, Injectable, OnInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MenuItem } from "primeng/api";
import { InputTextareaModule } from "primeng/inputtextarea";
import { InputTextModule } from "primeng/inputtext";
import { AutoCompleteModule } from "primeng/autocomplete";
import { FieldsetModule } from "primeng/fieldset";
import { GMapModule } from "primeng/gmap";
import { CheckboxModule } from "primeng/checkbox";
import { MessageService } from "primeng/api";
import { Contacts } from "../models/contacts";

import { ToastMessageService } from "../../shared/toast-message/toast-message.service";
import { ContactsService } from "../services/contacts/contacts.service";
import { CustomHtmlLabelService } from "../../shared/customHtmlLabel/custom-html-label.service";

import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
declare var google: any;

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  providers: [ContactsService, CustomHtmlLabelService],
  styleUrls: ["./contact.component.css"],
})
export class ContactComponent implements OnInit {
  /**
   * All map variable
   */

  public google: any;
  public overlays: any[];
  public dialogVisible: boolean;
  public markerTitle: string;
  public selectedPosition: any;
  public infoWindow: any;
  public options: any;
  public map: any;
  public event: any;
  public draggable: boolean;

  /**
   * define contacts data array
   */

  public contacts: Contacts[];

  /**
   * All contacts variables
   */

  public contactForm: FormGroup;
  public firstname: FormControl;
  public lastname: FormControl;
  public email: FormControl;

  // Define for checking the success or failure response of the restfull api

  public isSuccess = "";

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private toast: ToastMessageService,
    private messageService: MessageService,
    private contactsService: ContactsService,
    public customHtmlLabelService: CustomHtmlLabelService
  ) {}

  ngOnInit(): void {
    this.options = {
      center: { lat: 36.890257, lng: 30.707417 },
      zoom: 12,
    };

    this.initOverlays();

    this.infoWindow = new google.maps.InfoWindow();

    // get the all existing contacts

    this.getContacts();

    /**
     * This is for initializing the contactform
     */

    this.contactForm = new FormGroup({
      firstname: new FormControl(""),
      lastname: new FormControl(""),
      email: new FormControl(""),
      message: new FormControl(""),
    });
  }

  /**
   * This is use for getting the all  existing contacts
   */

  getContacts(): void {
    // call the restfull api for getting all existing contacts
    this.contactsService.getAllContacts().subscribe(
      (contacts) => {
        this.isSuccess = "true";
        // push the retriving contacts into contacts array
        this.contacts = contacts;
        // display toast message
        this.showToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showToast(this.isSuccess);
      }
    );
  }

  /**
   * Method for submitting the contact form
   * This add a new contact into the contacts array
   */

  onSubmit(contact: Contacts): void {
    // get the contactForm value

    contact = this.contactForm.value;
    // call the restfull api for getting all existing contacts
    this.contactsService.addContact(contact).subscribe(
      (newContact) => {
        this.isSuccess = "true";
        // push the new contact into contacts array
        this.contacts.push(newContact);
        // reset the contact form after submitting
        this.contactForm.reset();
        //display toast message
        this.showToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // reset the contact form after submitting
        this.contactForm.reset();
        // display toast message
        this.showToast(this.isSuccess);
      }
    );
  }

  /**
   *
   * @param event
   * All google maps method
   */

  handleMapClick(event) {
    this.dialogVisible = true;
    this.selectedPosition = event.latLng;
  }

  handleOverlayClick(event) {
    let isMarker = event.overlay.getTitle != undefined;

    if (isMarker) {
      let title = event.overlay.getTitle();
      this.infoWindow.setContent("" + title + "");
      this.infoWindow.open(event.map, event.overlay);
      event.map.setCenter(event.overlay.getPosition());

      this.messageService.add({
        severity: "info",
        summary: "Marker Selected",
        detail: title,
      });
    } else {
      this.messageService.add({
        severity: "info",
        summary: "Shape Selected",
        detail: "",
      });
    }
  }

  // This is use for adding new marker

  addMarker() {
    this.overlays.push(
      new google.maps.Marker({
        position: {
          lat: this.selectedPosition.lat(),
          lng: this.selectedPosition.lng(),
        },
        title: this.markerTitle,
        draggable: this.draggable,
      })
    );
    this.markerTitle = "";
    this.dialogVisible = false;
  }

  /**
   *
   * @param event
   *
   * This is use for handling the drag and drop
   */
  handleDragEnd(event) {
    this.messageService.add({
      severity: "info",
      summary: "Marker Dragged",
      detail: event.overlay.getTitle(),
    });
  }

  /**
   * This is use for init the map
   *
   */
  initOverlays() {
    if (!this.overlays || !this.overlays.length) {
      this.overlays = [
        new google.maps.Marker({
          position: { lat: 36.879466, lng: 30.667648 },
          title: "Konyaalti",
        }),
        new google.maps.Marker({
          position: { lat: 36.883707, lng: 30.689216 },
          title: "Ataturk Park",
        }),
        new google.maps.Marker({
          position: { lat: 36.885233, lng: 30.702323 },
          title: "Oldtown",
        }),
        new google.maps.Polygon({
          paths: [
            { lat: 36.9177, lng: 30.7854 },
            { lat: 36.8851, lng: 30.7802 },
            { lat: 36.8829, lng: 30.8111 },
            { lat: 36.9177, lng: 30.8159 },
          ],
          strokeOpacity: 0.5,
          strokeWeight: 1,
          fillColor: "#1976D2",
          fillOpacity: 0.35,
        }),
        new google.maps.Circle({
          center: { lat: 36.90707, lng: 30.56533 },
          fillColor: "#1976D2",
          fillOpacity: 0.35,
          strokeWeight: 1,
          radius: 1500,
        }),
        new google.maps.Polyline({
          path: [
            { lat: 36.86149, lng: 30.63743 },
            { lat: 36.86341, lng: 30.72463 },
          ],
          geodesic: true,
          strokeColor: "#FF0000",
          strokeOpacity: 0.5,
          strokeWeight: 2,
        }),
      ];
    }
  }

  /**
   *
   * @param map
   * This is use for zoom in
   */

  zoomIn(map) {
    map.setZoom(map.getZoom() + 1);
  }

  /**
   *
   * @param map
   * Thisq is use for zoom out
   */

  zoomOut(map) {
    map.setZoom(map.getZoom() - 1);
  }

  /**
   * This is use for deleting all marker
   */

  clear() {
    this.overlays = [];
  }

  /**
   * this is use for displaying  toast message
   */
  showToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.contacts_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.contacts_failure);
    }
  }
}
