import { Component, OnInit } from "@angular/core";
import { MenubarModule } from "primeng/menubar";
import { MenuItem } from "primeng/api";
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";
import { SharedModule } from "primeng/api";
import { ToolbarModule } from "primeng/toolbar";
import { SplitButtonModule } from "primeng/splitbutton";
import { FieldsetModule } from "primeng/fieldset";
import { SelectItem } from "primeng/api";
import { MessageService } from "primeng/api";
import { ToastModule } from "primeng/toast";
import { RippleModule } from "primeng/ripple";
import { PrimeNGConfig } from "primeng/api";
import { TableModule } from "primeng/table";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";

import { CustomHtmlLabelService } from "../../shared/customHtmlLabel/custom-html-label.service";

@Component({
  selector: "app-app-toolbar",
  templateUrl: "./app-toolbar.component.html",
  styleUrls: ["./app-toolbar.component.css", "../../shared/global.css"],
  providers: [MessageService, CustomHtmlLabelService],
  styles: [
    `
      :host ::ng-deep .p-cell-editing {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    `,
  ],
})
export class AppToolbarComponent implements OnInit {
  public items: MenuItem[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private primengConfig: PrimeNGConfig,
    public customHtmlLabelService: CustomHtmlLabelService
  ) {}

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.items = [
      {
        label: "Update",
        icon: "pi pi-refresh",
      },
      {
        label: "Save",
        icon: "pi pi-check",
      },
      {
        label: "Delete",
        icon: "pi pi-times",
      },
      {
        label: "Router",
        icon: "pi pi-upload",
        routerLink: "/fileupload",
      },
    ];
  }

  /**
   * Reset login status
   *  Call  logout() method into the authenticationService
   * */
  disConnect() {
    //Get return url from route parameters or default to '/first'
    this.router.navigate(["/first"]);
  }
  /**
   * Login if the user click on the button
   * submiting the login request
   */
  handleClick() {
    //execute action
  }

  /**
   * This for redirecting to home component
   *
   *
   */
  connexion() {
    //Get return url from route parameters or default to '/home
    this.router.navigate(["/home"]);
  }

  /**
   * This for redirecting to first page component
   *
   *
   */
  goHome() {
    //Get return url from route parameters or default to '/first
    this.router.navigate(["/first"]);
  }

  /**
   * This for redirecting to  admin component
   *
   *
   */
  manageUser() {
    //Get return url from route parameters or default to '/admin
    this.router.navigate(["/admin"]);
  }
  /**
   * This for redirecting to report component
   *
   *
   */
  getReport() {
    //Get return url from route parameters or default to '/report
    this.router.navigate(["/report"]);
  }
  /**
   * This for redirecting to map component
   *
   *
   */
  getMap() {
    //Get return url from route parameters or default to '/map
    this.router.navigate(["/map"]);
  }
  /**
   * Reset login status
   *  Call  back method redirection to home page
   * */
  back() {
    //Get return url from route parameters or default to '/first'
    this.router.navigate(["/first"]);
  }

  /**
   * Reset login status
   *  Call  back method redirection to home page
   * */
  feedBack() {
    //Get return url from route parameters or default to '/contact'
    this.router.navigate(["/contact"]);
  }
}
