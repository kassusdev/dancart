import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { InputTextModule } from "primeng/inputtext";
import { ButtonModule } from "primeng/button";
import { SharedModule } from "primeng/api";
import { CustomHtmlLabelService } from "../../shared/customHtmlLabel/custom-html-label.service";
import { ChartModule } from "primeng/chart";
import { PanelModule } from "primeng/panel";
import { PrimeNGConfig } from "primeng/api";
import { MessageService } from "primeng/api";

import { Users } from "../models/users";
import { Cards } from "../models/cards";
import { Reports } from "../models/reports";

import { ToastMessageService } from "../../shared/toast-message/toast-message.service";
import { ReportsService } from "../services/reports/reports.service";
import { UsersService } from "../services/users/users.service";

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.css", "../../shared/global.css"],
  providers: [
    UsersService,
    ReportsService,
    MessageService,
    CustomHtmlLabelService,
  ],
})
export class ReportComponent implements OnInit {
  // Define data array using for display the all charts models
  public dataUsers: any;
  public dataNbCards: any;

  // Define custom array for users and cards

  public users: Users[];
  public cards: Cards[];

  // Define for checking the success or failure response of the restfull api

  public isSuccess = "";

  // Define an reports array

  public reports: Reports[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private toast: ToastMessageService,
    public customHtmlLabelService: CustomHtmlLabelService,
    private usersService: UsersService,
    private reportsService: ReportsService,
    private primengConfig: PrimeNGConfig
  ) {}

  ngOnInit() {
    this.getAllReports();

    // This is a fake data for testing the display of the tree chart model
    this.dataUsers = {
      labels: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
      ],
      datasets: [
        {
          label: "Nombre de prof",
          backgroundColor: "#42A5F5",
          borderColor: "#1E88E5",
          data: [65, 59, 80, 81, 56, 55, 60, 70],
        },
        {
          label: "Nombre d'eleve",
          backgroundColor: "#9CCC65",
          borderColor: "#7CB342",
          data: [28, 48, 40, 19, 86, 27, 50, 80],
        },
      ],
    };

    this.dataNbCards = {
      datasets: [
        {
          data: [11, 16, 7, 3, 14, 25],
          backgroundColor: [
            "#FF6384",
            "#4BC0C0",
            "#FFCE56",
            "#E7E9ED",
            "#36A2EB",
          ],
          label: "My dataset",
        },
      ],
      labels: ["Red", "Green", "Yellow", "Grey", "Blue"],
    };
  }

  /**
   * This is use for getting the all  existing users
   */

  getAllReports(): void {
    // call the restfull api for getting all existing users
    this.reportsService.getAllReports().subscribe(
      (reports) => {
        this.isSuccess = "true";
        // push the retriving users into users array
        this.reports = reports;
        // add all nbteachers into the charts data array
        for (let i = 0; i < this.reports.length; i++) {
          const report = this.reports[i];
          if (report.nbteachers.length > 0) {
            // this will be use for nbteachers
            this.dataUsers.datasets[0].data.push(report.nbteachers.length);
            // this will be use for nbusers
            this.dataUsers.datasets[1].data.push(report.nbusers.length);
            // this will be use for nbcards
            this.dataNbCards.datasets[0].data.push(report.nbcards.length);
            // this will be use for nbcourses
            this.dataNbCards.datasets[1].data.push(report.nbcourses.length);
          }
        }
        // display toast message
        this.showToast(this.isSuccess);
      },
      (error) => {
        this.isSuccess = "false";
        // display toast message
        this.showToast(this.isSuccess);
      }
    );
  }

  /**
   * this is use for displaying  toast message
   */
  showToast(toastCheck: string) {
    toastCheck = this.isSuccess;
    if (toastCheck === "true") {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.reports_success);
    } else {
      // the messagerieservice display custom toast message
      this.messageService.add(this.toast.ToastMessage.reports_failure);
    }
  }
}
